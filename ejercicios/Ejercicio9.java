package webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Ejercicio9 {
    public static void main(String[] args){
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] a = new int[5];

        for (int i=0; i<a.length;i++){
            try {
                System.out.println((i+1)+") ingresa un valor: ");
                a[i] = Integer.parseInt(br.readLine());
            } catch (IOException e) {
                System.out.println("Ingresaste un numero invalido por defecto usaremos "+(i+1));
                a[i] = i;
            }
        }

        System.out.println("For");
        for (int i = 0; i < a.length; i++){
            System.out.print(a[i] + ", ");
        }

        System.out.println();
        System.out.println("Foreach");
        for (int i:a){
            System.out.print(i + ", ");
        }

        System.out.println();
        System.out.println("Streams");
        Arrays.stream(a).forEach(ints -> System.out.print(ints + ", "));

    }
}
