package webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Ejercicio10 {

    public static void main(String[] args){
        Ejercicio10 ejercicio10 = new Ejercicio10();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int length = 0;
        int multiple = 0;
        try {
            System.out.println("Ingresa la longitud del arreglo: ");
            length = Integer.parseInt(br.readLine());
        } catch (IOException e) {
            System.out.println("Ingresaste un numero invalido por defecto usaremos "+10);
            length = 10;
        }
        try {
            System.out.println("Ingresa el valor multiplo: ");
            multiple = Integer.parseInt(br.readLine());
        } catch (IOException e) {
            System.out.println("Ingresaste un numero invalido por defecto usaremos "+3);
            multiple = 3;
        }
        int[] array = ejercicio10.createArray(length, multiple);
        ejercicio10.print(array);
    }

    private int[] createArray(int length, int multiple){
        int[] array = new int[length];
        for (int i=0; i<length; i++){
            array[i] = multiple * (i+1);
        }
        return array;
    }

    private void print(int [] array){
        for (int i:array){
            System.out.print(i + ", ");
        }
    }
}
