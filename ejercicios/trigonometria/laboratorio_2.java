/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trigonometria;
import java.util.Scanner;
/**
 *
 * @author MauRi
 */
public class laboratorio_2 {
    public static void main(String[] args) {    
        //Cree e inicialice dos objetos de tipo Punto y muestre la distancia entre ellos.
        Punto p1 = new Punto(2,3);
        Punto p2 = new Punto(5,9);
        System.out.println ("1) Distancia entre p1 y p2: "+p1.calcularDistancia(p2));
        
        //Cree un objeto de la clase Circulo y muestre su área, su perímetro y la
        //distancia entre el Círculo y uno de los Puntos creados en el punto anterior
        Circulo circulo = new Circulo(3,4,2);
        System.out.println ("2) Area del circulo: "+circulo.calcularArea());
        System.out.println ("   Perimetro del circulo: "+circulo.calcularPerimetro());
        System.out.println ("   Distancia entre circulo y p1: "+ circulo.calcularDistancia(p1));
        
        //Cree un objeto de la clase Triangulo y muestre su área, su perímetro y la
        //distancia entre el Triángulo y un nuevo Puntos.
        Triangulo triangulo = new Triangulo(3,4,6,7,9,10);
        Punto p3 = new Punto(-2,3);
        System.out.println ("3) Area del triangulo: "+triangulo.calcularArea());
        System.out.println ("   Perimetro del triangulo: "+triangulo.calcularPerimetro());
        System.out.println ("   Distancia entre triangulo y p3:"+triangulo.calcularDistancia(p3));
    }
}
