package webservices;

import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;

public class Ejercicio5 {
    public static void main(String[] args){
        int maxPrimeNumber = 1000;
        List<Integer> primeNumber = new ArrayList<>(Arrays.asList(2));
        allNumbers: for (int i=3; i < maxPrimeNumber; i = i +2){
            for(int j=1; j < primeNumber.size() / 2; j++ ){
                if(i % primeNumber.get(j) == 0){
                    continue allNumbers;
                }
            }
            primeNumber.add(i);
        }
        System.out.println("Numeros primos");
        primeNumber.stream().forEach(prime -> System.out.print(prime + ", ") );

        System.out.println("Años bisiestos entre 2000 y 3000 ");
        IntStream.rangeClosed(2000, 3000).filter(year -> Year.of(year).isLeap()).forEach(year ->  System.out.print(year + ", "));
    }
}
