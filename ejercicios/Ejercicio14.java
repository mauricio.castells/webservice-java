package webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio14 {
    public static void main(String[] args) {
        Ejercicio10 ejercicio10 = new Ejercicio10();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int number = 0;
        try {
            System.out.println("Ingresa un numero: ");
            number = Integer.parseInt(br.readLine());
        } catch (IOException e) {
            System.out.println("Ingresaste un numero invalido por defecto usaremos " + 10);
            number = 10;
        }
        System.out.print("El valor ASCII es: " +Character.toString ((char) number));

    }
}
