import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumerosPrimos
{
    public static void main(String[] args) throws IOException
    {
	     for(int i=2;i<=100;i++)
	     {
		if(esPrimo(i))
		{
			System.out.println("Es primo "+i);
		}	     
	     }
	     for(int i=2000;i<=2020;i++)
	     {
		if(esBisiesto(i))
		{
			System.out.println("Es anio bisiesto "+i);
		}	     
	     }
	}
	public static boolean esPrimo(int numero){
	  int contador = 2;
	  boolean primo=true;
	  while ((primo) && (contador!=numero)){
	    if (numero % contador == 0)
	      primo = false;
	    contador++;
	  }
	  return primo;
	}
	
	public static boolean esBisiesto(int anio){
		boolean bisiesto=false;
		if ((anio % 4 == 0) && ((anio % 100 != 0) || (anio % 400 == 0)))
			bisiesto=true;
		return bisiesto;
	}
}	