//http://www.browxy.com/

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Factoriales
{
    public static void main(String[] args) throws IOException
    {
    	int numero = 40;
	System.out.println(factorial(numero));
	}
	
	public int factorial (int numero) {
		if (numero==0)
			return 1;
		else
			return numero * factorial(numero-1);
	}
}	