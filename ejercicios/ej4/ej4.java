import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Mayorymenor
{
    public static void main(String[] args) throws IOException
    {
        //Notar que readLine() nos obliga a declarar IOException
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in)); //Ya tenemos el "lector"

        System.out.println("Ingrese numero 1");//Se pide un dato al usuario

        String numero = br.readLine(); //Se lee el nombre con readLine() que retorna un String con el dato

        int N1 = Integer.parseInt(numero);//Se transforma la entrada anterior en un entero
        //Si el usuario ingresó solo números funcionará bien, de lo contrario generará una excepción

        System.out.println("Ingrese numero 2");//Se pide otro dato al usuario

        numero = br.readLine(); //Se guarda la entrada (edad) en una variable

        int N2 = Integer.parseInt(numero);//Se transforma la entrada anterior en un entero
        //Si el usuario ingresó solo números funcionará bien, de lo contrario generará una excepción
	if (N1 > N2){
            System.out.println("N1 mayor "+N1);
        }else if (N1 < N2){
            System.out.println("N2 mayor "+N2);
        }else{
            System.out.println("iguales");
        }


	}
}	