package webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Ejercicio12 {
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] a = new int[10];

        for (int i = 0; i < a.length; i++) {
            try {
                System.out.println((i + 1) + ") ingresa un valor: ");
                a[i] = Integer.parseInt(br.readLine());
            } catch (IOException e) {
                System.out.println("Ingresaste un numero invalido por defecto usaremos " + (i + 1));
                a[i] = i;
            }
        }
        double averagePositive = Arrays.stream(a).filter(value -> value >= 0 ).average().getAsDouble();
        System.out.println("Promedio valores Positivos " + averagePositive);
        double averageNegative = Arrays.stream(a).filter(value -> value < 0 ).average().getAsDouble();
        System.out.println("Promedio valores Negativos " + averageNegative);
    }
}