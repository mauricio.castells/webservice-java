package webservices;

public class Ejercicio3 {

    public static void main(String[] args){
        float a = 20;
        float b = 5;

        if(args != null){
            a = args.length > 0 ? Float.parseFloat(args[0]): a;
            b = args.length > 1 ? Float.parseFloat(args[1]): b;
        }
        System.out.println(String.format("%.2f mas %.2f = %.2f",a,b,(a+b)));
        System.out.println(String.format("%.2f menos %.2f = %.2f",a,b,(a-b)));
        System.out.println(String.format("%.2f por %.2f = %.2f",a,b,(a*b)));
        System.out.println(String.format("%.2f dividido %.2f = %.2f",a,b,(a/b)));
        System.out.println(String.format("%.2f modulo %.2f = %.2f",a,b,(a%b)));
    }


}
