package webservices;

public class Ejercicio4 {
    public static void main(String[] args){
        int a = 20;
        int b = 5;

        if(args != null){
            a = args.length > 0 ? Integer.parseInt(args[0]): a;
            b = args.length > 1 ? Integer.parseInt(args[1]): b;
        }

        if(a == b){
            System.out.println(String.format("%d y %d son iguales", a, b));
        } else {
            System.out.print(String.format("%d es mayor que %d", Math.max(a,b), Math.min(a,b)));
        }
    }
}
