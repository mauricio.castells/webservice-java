package webservices;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Ejercicio8 {
    public static void main(String[] args){
        int[] a = {1,2,3,4,5};

        System.out.println("For");
        for (int i = 0; i < a.length; i++){
            System.out.print(a[i] + ", ");
        }

        System.out.println();
        System.out.println("Foreach");
        for (int i:a){
            System.out.print(i + ", ");
        }

        System.out.println();
        System.out.println("Streams");
        Arrays.stream(a).forEach(ints -> System.out.print(ints + ", "));

    }
}
