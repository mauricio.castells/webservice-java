package webservices;

import java.util.stream.IntStream;

public class Ejercicio7 {
    public static void main(String[] args){
        IntStream.rangeClosed(1,100).reduce((x, y) -> x + y).
                ifPresent(value -> System.out.print(value));

    }
}
