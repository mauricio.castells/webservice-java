package webservices;

import java.util.stream.IntStream;

public class Ejercicio6 {
    public static void main(String[] args){
        int a = 5;

        if(args != null){
            a = args.length > 0 ? Integer.parseInt(args[0]): a;
        }

        IntStream.rangeClosed(1,a).reduce((x, y) -> Math.multiplyExact(x,y)).
                ifPresent(value -> System.out.print(value));

    }
}
