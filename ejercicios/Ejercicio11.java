package webservices;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Ejercicio11 {
    public static void main(String[] args) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int[] a = new int[10];

        int sum = 0;
        int totalEven = 0;
        for (int i = 0; i < a.length; i++) {
            try {
                System.out.println((i + 1) + ") ingresa un valor: ");
                a[i] = Integer.parseInt(br.readLine());
            } catch (IOException e) {
                System.out.println("Ingresaste un numero invalido por defecto usaremos " + (i + 1));
                a[i] = i;
            }
            if (i % 2 == 0){
                sum += a[i];
                totalEven++;
            }
        }

        System.out.println("La media de los numeros en posicion par es: " + (sum/totalEven));
    }
}
